@extends('layouts.layout', ['title' => "Пост №$post->id.$post->title"])

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-header"><h2>{{$post->title}}</h2></div>
            <div class="card-body">
                <div class="card-img card-img_max"
                     style="background-image: url({{$post->img ?? asset('img/default.jpg')}})"></div>
                <div class="card">{{$post->description}}</div>
                <div class="card-author">Автор: {{$post->name}}</div>
                <div class="card-author">Время: {{$post->created_at->diffForHumans()}}</div>
                <a href="{{route('post.index')}}" class="btn btn-outline-primary">На главную</a>
                <a href="{{route('post.edit', ['post'=>$post->post_id])}}"
                   class="btn btn-outline-success">Редактировать</a>
                <form action="{{route('post.destroy', ['post'=>$post->post_id])}}" method="post"
                      onsubmit="if(confirm('Точно удалить пост?')) {return true} else {return false}">
                    @csrf
                    @method('delete')

                    <input type="submit" class="btn-outline-danger" value="Удалить">
                </form>
            </div>
        </div>
    </div>
@endsection
