@extends('layouts.layout', ['title' => "Редач №$post->id.$post->title"])

@section('content')
    <form action="{{route('post.update', ['post'=>$post->post_id])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('patch')
        <h3>Изменить пост</h3>
        @include('posts.parts.form')

        <input type="submit" value="Редактировать пост" class="btn btn-outline-success">
    </form>
@endsection
