<div class="form-group">
    <label>
        <input name="title" type="text" class="form-control" required value="{{ old('title') ?? $post->title ?? ''}}">
    </label>
</div>
<div class="form-group">
    <label>
        <textarea name="description" rows="10" class="form-control" required>{{old('description') ?? $post->description ?? ''}}</textarea>
    </label>
</div>
<div class="form-group">
    <input type="file" name="img">
</div>
