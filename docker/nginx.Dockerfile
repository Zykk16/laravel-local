FROM nginx:1.15.12-alpine
ARG DOCKER_GROUP_ID
ARG DOCKER_USER_ID
ARG HOSTNAME
ENV DOCKER_GROUP_ID ${DOCKER_GROUP_ID}
ENV DOCKER_USER_ID ${DOCKER_USER_ID}
ENV HOSTNAME ${HOSTNAME}

RUN mkdir /etc/nginx/sites-available
ADD nginx/nginx.conf /etc/nginx
ADD nginx/sites-available/www /etc/nginx/sites-available/$HOSTNAME
RUN addgroup -g $DOCKER_GROUP_ID www-data && \
    adduser -G www-data -D -H -h /var/www/public -u $DOCKER_USER_ID www-data
RUN mkdir /var/www && mkdir /var/www/public && \
    chown -R www-data:www-data /var/www/public
RUN sed -i "s/HOSTNAME/${HOSTNAME}/g" /etc/nginx/sites-available/$HOSTNAME && \
    mkdir /etc/nginx/sites-enabled && \
    ln -s /etc/nginx/sites-available/$HOSTNAME /etc/nginx/sites-enabled/$HOSTNAME

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
