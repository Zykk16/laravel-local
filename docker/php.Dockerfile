FROM php:7.4.0-fpm-alpine3.10

ARG DOCKER_GROUP_ID
ARG DOCKER_USER_ID
ENV DOCKER_GROUP_ID ${DOCKER_GROUP_ID}
ENV DOCKER_USER_ID ${DOCKER_USER_ID}
RUN apk --no-cache add shadow && usermod -u $DOCKER_USER_ID www-data && groupmod -g $DOCKER_GROUP_ID www-data

RUN apk add --no-cache --virtual .build-deps \
    oniguruma-dev \
    build-base \
    autoconf \
    libxml2-dev \
    libzip-dev \
    freetype \
    icu-dev \
  && \
  apk add --no-cache imagemagick-dev imagemagick \
  && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
  && \
  docker-php-ext-install -j${NPROC} \
    exif \
    intl \
    pcntl \
    bcmath \
    iconv \
    json \
    mbstring \
    pdo \
    pdo_mysql \
    mysqli \
    zip \
    xml \
  && \
  pecl install xdebug imagick \
    && docker-php-ext-enable xdebug imagick \
  && \
  runDeps="$( \
    scanelf --needed --nobanner --recursive /usr/local \
    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
    | sort -u \
    | xargs -r apk info --installed \
    | sort -u \
    )" \
  && \
  apk del .build-deps \
  && \
  apk add --no-cache git npm ffmpeg $runDeps \
  && \
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --version=1.10.19 --install-dir=/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

WORKDIR /var/www
RUN mkdir /var/www/storage && chown -R www-data:www-data /var/www

EXPOSE 8000
CMD [ "php-fpm" ]
