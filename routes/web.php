<?php

Route::get('/', 'PostController@index');

Route::resource('post', 'PostController');

Auth::routes();
